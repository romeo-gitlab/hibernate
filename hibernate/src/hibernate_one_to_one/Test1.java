package hibernate_one_to_one;


import hibernate_one_to_one.entity.Detail;
import hibernate_one_to_one.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test1 {
    public static void main(String[] args) {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Detail.class)
                .buildSessionFactory();

        Session session = null;

        try {
            session = factory.getCurrentSession();
            //Employee emp = new Employee("Rahim", "Plugov", "Cleaners", 350);

            Detail detail = new Detail("Himki", "111525356", "plug@mail.ru");

            session.beginTransaction();
            Employee emp = session.get(Employee.class, 9);
            emp.setEmpDetail(detail);
            session.save(emp);
            session.getTransaction().commit();
        }
        finally {
            session.close();
            factory.close();
        }

    }
}
