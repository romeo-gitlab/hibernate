package hibernate_one_to_one;


import hibernate_one_to_one.entity.Detail;
import hibernate_one_to_one.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test2 {
    public static void main(String[] args) {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Detail.class)
                .buildSessionFactory();

        Session session = null;

        try {
            session = factory.getCurrentSession();
            Employee emp = new Employee("Misha", "Bodrov", "HR", 850);

            Detail detail = new Detail("Voronej", "750525356", "bodry@mail.ru");

            session.beginTransaction();

            emp.setEmpDetail(detail);
            detail.setEmployee(emp);
            session.save(detail);
            session.getTransaction().commit();
        }
        finally {
            session.close();
            factory.close();
        }

    }
}
