import hibernate_test.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Test3 {
    public static void main(String[] args) {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        try {
            Session session = factory.getCurrentSession();
            session.beginTransaction();

            List<Employee> emps = session.createQuery("from Employee").getResultList();
            for (Employee emp : emps) {
                System.out.println(emp);
            }

            System.out.println();

            List<Employee> emps2 = session.createQuery("from Employee " +
                    "where department = 'IT' and salary>700").getResultList();
            for (Employee emp : emps2) {
                System.out.println(emp);
            }

            session.getTransaction().commit();
            System.out.println("done");
        }
        finally {
            factory.close();
        }

    }
}
