package collections;

import java.util.ArrayList;
import java.util.Objects;

public class ArrayListAddAll {
    public static void main(String[] args) {
        ArrayList<String> arrayList1 = new ArrayList<>();
        arrayList1.add("Zaur");
        arrayList1.add("Ivan");
        arrayList1.add("Maria");
        ArrayList<String> arrayList2 = new ArrayList<>();
        arrayList2.add("!!!");
        arrayList2.add("???");
        arrayList2.addAll(1, arrayList1);
        System.out.println(arrayList2);
        arrayList2.clear();
        System.out.println(arrayList2);

    }


}