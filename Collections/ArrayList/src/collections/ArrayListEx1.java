package collections;

import java.util.ArrayList;
import java.util.List;

public class ArrayListEx1 {
    public static void main(String[] args) {
        ArrayList<String> arrayList1 = new ArrayList<>();
        arrayList1.add("Zaur");
        arrayList1.add("Ivan");
        arrayList1.add("Maria");
        arrayList1.add("50");
        System.out.println(arrayList1);
        ArrayList<String> arrayList2 = new ArrayList<>(20);
        List<String> arrayList3 = new ArrayList<>();
        arrayList3.add("Ivan");
        arrayList3.add("Maria");
        arrayList3.add(1, "Michail");
        List<String> arrayList4 = new ArrayList<>(arrayList3);
        for (String s:arrayList4) {
            System.out.print(s + " ");
        }
        System.out.println();
        System.out.println(arrayList4.get(2));
        for (int i = 0; i < arrayList1.size(); i++) {
            System.out.print(arrayList1.get(i) + " ");
        }
        System.out.println();
        arrayList1.set(3, "Ivan");
        arrayList1.remove(0);
        System.out.println(arrayList1.lastIndexOf("Ivan"));
        System.out.println(arrayList1);
        System.out.println(arrayList1.contains("Maria"));

    }

}
