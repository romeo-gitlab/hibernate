package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BinarySearch {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(-8);
        arrayList.add(-20);
        arrayList.add(-1);
        arrayList.add(0);
        arrayList.add(5);
        arrayList.add(-3);
        arrayList.add(10);
        arrayList.add(4);
        arrayList.add(25);
        arrayList.add(-18);
        arrayList.add(56);
        Collections.reverse(arrayList);
        System.out.println(arrayList);
        Collections.shuffle(arrayList);
        System.out.println(arrayList);
        Collections.sort(arrayList);
        System.out.println(arrayList);

        int index1 = Collections.binarySearch(arrayList, 10);
        System.out.println(index1);

        Employee employee1 = new Employee(100, "Ivan", 1100);
        Employee employee2 = new Employee(110, "Irina", 500);
        Employee employee3 = new Employee(50, "Igor", 900);
        Employee employee4 = new Employee(1, "Ilya", 1500);
        Employee employee5 = new Employee(1, "Isabel", 1300);
        Employee employee6 = new Employee(9, "Elisa", 1000);
        Employee employee7 = new Employee(19, "Imanbek", 950);
        List<Employee> emloyeeList = new ArrayList<>();
        emloyeeList.add(employee1);
        emloyeeList.add(employee2);
        emloyeeList.add(employee3);
        emloyeeList.add(employee4);
        emloyeeList.add(employee5);
        emloyeeList.add(employee6);
        emloyeeList.add(employee7);
        System.out.println(emloyeeList);
        Collections.sort(emloyeeList);
        System.out.println(emloyeeList);
        int index2 = Collections.binarySearch(emloyeeList, employee3);
        System.out.println(index2);

        int [] array = {-20, -18, -8, -3, -1, 0, 4, 5, 10, 25, 56};
        System.out.println(Arrays.toString(array));
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        int index3 = Arrays.binarySearch(array, 5) + 1;
        System.out.println(index3);

    }
      static class Employee implements Comparable<Employee>  {
        int id;
        String name;
        int salary;

        public Employee(int id, String name, int salary) {
            this.id = id;
            this.name = name;
            this.salary = salary;
        }

        @Override
        public String toString() {
            return "Employee{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", salary=" + salary +
                    '}';
        }

          @Override
          public int compareTo(Employee anotherEmp) {
             int result = this.id - anotherEmp.id;
             if (result == 0) {
                result = this.name.compareTo(anotherEmp.name);
             }
             return result;
          }
      }
}
