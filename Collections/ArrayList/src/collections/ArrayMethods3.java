package collections;

import java.util.ArrayList;
import java.util.List;

public class ArrayMethods3 {
    public static void main(String[] args) {
        ArrayList<String> arrayList1 = new ArrayList<>();
        arrayList1.add("Zaur");
        arrayList1.add("Ivan");
        arrayList1.add("Maria");
        arrayList1.add("Nick");
        arrayList1.add("Dick");
        System.out.println(arrayList1);
        List<String> myList = arrayList1.subList(1, 4);
        System.out.println(myList);
        
        myList.add("50-s");
        System.out.println();
        System.out.println(myList);
        System.out.println(arrayList1);
        ArrayList<String> arrayList2 = new ArrayList<>();
        arrayList2.add("Igor");
        arrayList2.add("Ivan");
        arrayList2.add("Maria");
        //arrayList1.removeAll(arrayList2);
        arrayList1.retainAll(arrayList2);
        System.out.println(arrayList1);
        boolean result = arrayList1.containsAll(arrayList2);
        System.out.println(result);

        Object [] array1 = arrayList1.toArray();
        String [] array2 = arrayList2.toArray(new String[0]);
        System.out.println();
        for (Object o : array1) {
            System.out.println(o);
        }
        System.out.println();
        for (String s : array2) {
            System.out.println(s);
        }

        //List<String> list6 = List.copyOf(arrayList1);

        //List<Integer> list5 = List.of(3, 10, 13);
        //System.out.println(list5);


    }
}
