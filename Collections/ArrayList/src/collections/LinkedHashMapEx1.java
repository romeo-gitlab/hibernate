package collections;

import java.util.LinkedHashMap;

public class LinkedHashMapEx1 {
    public static void main(String[] args) {
        LinkedHashMap<Double, HashCodeEx1.Student> linkedHashMap =
                new LinkedHashMap<>(16, 0.75f, true);
        HashCodeEx1.Student st1 = new HashCodeEx1.Student("Zaur", "Tregulov", 3);
        HashCodeEx1.Student st2 = new HashCodeEx1.Student("Maria", "Ivanova", 4);
        HashCodeEx1.Student st3 = new HashCodeEx1.Student("Sergey", "Petrov", 1);
        HashCodeEx1.Student st4 = new HashCodeEx1.Student("Lesha", "Sydorov", 2);
        linkedHashMap.put(5.8, st3);
        linkedHashMap.put(6.8, st2);
        linkedHashMap.put(9.2, st1);
        linkedHashMap.put(7.8, st4);
        System.out.println(linkedHashMap.get(6.8));
        System.out.println(linkedHashMap);


    }
}
