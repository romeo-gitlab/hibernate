package collections;

import java.util.TreeMap;

public class TreeMapEx1 {
    public static void main(String[] args) {
        TreeMap<Double, HashCodeEx1.Student> treeMap = new TreeMap<>();
        HashCodeEx1.Student student1 = new HashCodeEx1.Student("Zaur", "Tregulov", 3);
        HashCodeEx1.Student student2 = new HashCodeEx1.Student("Maria", "Ivanova", 4);
        HashCodeEx1.Student student3 = new HashCodeEx1.Student("Sergey", "Petrov", 1);
        HashCodeEx1.Student student4 = new HashCodeEx1.Student("Lesha", "Sydorov", 2);
        HashCodeEx1.Student student5 = new HashCodeEx1.Student("Vadim", "Popov", 3);
        HashCodeEx1.Student student6 = new HashCodeEx1.Student("Pavel", "Petushkov", 5);
        HashCodeEx1.Student student7 = new HashCodeEx1.Student("Max", "Pavlov", 1);
        treeMap.put(5.8, student1);
        treeMap.put(6.8, student2);
        treeMap.put(9.2, student3);
        treeMap.put(7.8, student4);
        treeMap.put(8.4, student5);
        treeMap.put(8.0, student6);
        treeMap.put(4.9, student7);
        System.out.println(treeMap);
        System.out.println(treeMap.get(9.2));
        treeMap.remove(6.8);
        System.out.println(treeMap);
        System.out.println(treeMap.descendingMap());
        System.out.println(treeMap.tailMap(7.8));
        System.out.println(treeMap.headMap(7.8));
        System.out.println(treeMap.lastEntry());
        System.out.println(treeMap.firstEntry());

    }

}
