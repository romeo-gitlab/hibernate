package collections;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorEx {
    public static void main(String[] args) {
        ArrayList<String> arrayList1 = new ArrayList<>();
        arrayList1.add("Zaur");
        arrayList1.add("Ivan");
        arrayList1.add("Maria");
        arrayList1.add("Nick");
        arrayList1.add("Dick");
        Iterator<String> iterator = arrayList1.iterator();
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next());
//        }
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
        System.out.println(arrayList1);
    }
}
