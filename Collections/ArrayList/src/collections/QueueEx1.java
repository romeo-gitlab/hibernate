package collections;

import java.util.LinkedList;
import java.util.Queue;

public class QueueEx1 {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();
        queue.add("Zaur");//.offer - то же самое но не выбрасывает эксепшн
        queue.add("Ivan");
        queue.add("Izaura");
        queue.add("Izaura");
        System.out.println(queue);
        System.out.println(queue.remove());//.poll - то же самое но не выбрасывает эксепшн
        System.out.println(queue);
        System.out.println(queue.element());//.pick - то же самое но не выбрасывает эксепшн
    }
}

