package collections;

import java.util.Objects;
import java.util.PriorityQueue;

public class PriorityQueueEx1 {
    public static void main(String[] args) {
        PriorityQueue<Student> priorityQueue = new PriorityQueue<>();
        Student student1 = new Student("Zaur", 3);
        Student student2 = new Student("Maria", 5);
        Student student3 = new Student("Sergey", 1);
        priorityQueue.add(student1);
        priorityQueue.add(student3);
        priorityQueue.add(student2);
        System.out.println(priorityQueue);
    }
    static class Student implements Comparable<Student> {
        String name;
        int course;

        public Student(String name, int course) {
            this.name = name;
            this.course = course;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Student student = (Student) o;
            return course == student.course; //сравнение имен убрали чтоб соблюдалось правило иквелс=тру, компэр=0
        }

        @Override
        public int hashCode() {
            return Objects.hash(course); //тоже убрали нэйм
        }

        @Override
        public int compareTo(Student other) {
            return this.course - other.course;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", course=" + course +
                    '}';
        }
    }
}
