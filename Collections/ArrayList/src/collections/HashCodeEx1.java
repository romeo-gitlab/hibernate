package collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class HashCodeEx1 {
    public static void main(String[] args) {
        Map<Student, Double> map = new HashMap<>();
        Student student1 = new Student("Zaur", "Tregulov", 3);
        Student student2 = new Student("Maria", "Ivanova", 4);
        Student student3 = new Student("Sergey", "Petrov", 1);
        map.put(student1, 7.8);
        map.put(student2, 6.8);
        map.put(student3, 8.8);
        System.out.println(map);
        Student student4 = new Student("Zaur", "Tregulov", 3);
        boolean result = map.containsKey(student4);
        System.out.println("result= " + result);
        System.out.println(student1.hashCode());
        System.out.println(student4.hashCode());
        for (Map.Entry<Student, Double> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
        Map<Integer, String> map2 = new HashMap<>(16, 0.75f);

    }
//рекомендуется делать файнал - чтобы не менялся хэшкод
    static final class Student {
        final String name;
        final String surname;
        final int course;

        public Student(String name, String surname, int course) {
            this.name = name;
            this.surname = surname;
            this.course = course;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", course=" + course +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Student student = (Student) o;
            return course == student.course && Objects.equals(name, student.name) && Objects.equals(surname, student.surname);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, surname, course);
        }
        /*@Override
        public int hashCode() {
            return name.length()*7 + surname.length()*11 + course*53;
        }*/
    }
}
