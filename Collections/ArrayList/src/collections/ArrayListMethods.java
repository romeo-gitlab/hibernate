package collections;

import java.util.ArrayList;
import java.util.Objects;

public class ArrayListMethods {
    public static void main(String[] args) {
        Student2 student1 = new Student2("Alex", 'm', 22, 3, 8.3);
        Student2 student2 = new Student2("Phil", 'm', 23, 4, 5.3);
        Student2 student3 = new Student2("Sarah", 'f', 20, 2, 6.3);
        Student2 student4 = new Student2("Joe", 'm', 19, 1, 7.3);
        Student2 student5 = new Student2("Alexia", 'f', 22, 3, 8.0);
        ArrayList<Student2> studentList = new ArrayList<>();
        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        studentList.add(student4);
        studentList.add(student5);
        for (Student2 student : studentList) {
            System.out.println(student);
        }
        System.out.println();
        Student2 student6 = new Student2("Alexia", 'f', 22, 3, 8.0);
        //studentList.remove(student6);
        for (Student2 student : studentList) {
            System.out.println(student);
        }
        int index = studentList.indexOf(student6);
        System.out.println(index);

    }

    static class Student2 {
        private String name;
        private char sex;
        private int age;
        private int course;
        private double avgGrade;

        public Student2(String name, char sex, int age, int course, double avgGrade) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.course = course;
            this.avgGrade = avgGrade;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", sex=" + sex +
                    ", age=" + age +
                    ", course=" + course +
                    ", avgGrade=" + avgGrade +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Student2 student = (Student2) o;
            return sex == student.sex &&
                    age == student.age &&
                    course == student.course &&
                    Double.compare(student.avgGrade, avgGrade) == 0 &&
                    Objects.equals(name, student.name);
        }

    }

}

