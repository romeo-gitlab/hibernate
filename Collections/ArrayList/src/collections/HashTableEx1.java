package collections;

import java.util.Hashtable;

public class HashTableEx1 {
    public static void main(String[] args) {
        Hashtable<Double, HashCodeEx1.Student> hashtable = new Hashtable<>();
        HashCodeEx1.Student st1 = new HashCodeEx1.Student("Zaur", "Tregulov", 3);
        HashCodeEx1.Student st2 = new HashCodeEx1.Student("Maria", "Ivanova", 4);
        HashCodeEx1.Student st3 = new HashCodeEx1.Student("Sergey", "Petrov", 1);
        HashCodeEx1.Student st4 = new HashCodeEx1.Student("Lesha", "Sydorov", 2);
        hashtable.put(5.8, st3);
        hashtable.put(6.8, st2);
        hashtable.put(9.2, st1);
        hashtable.put(7.8, st4);
        System.out.println(hashtable);
    }
}
