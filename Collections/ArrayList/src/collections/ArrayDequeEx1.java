package collections;

import java.util.ArrayDeque;
import java.util.Deque;

public class ArrayDequeEx1 {
    public static void main(String[] args) {
        Deque<Integer> arrayDeque = new ArrayDeque<>();
        arrayDeque.addFirst(3);
        arrayDeque.addFirst(5);
        arrayDeque.addLast(1);
        arrayDeque.offerLast(8);
        System.out.println(arrayDeque);
        arrayDeque.removeFirst();
        arrayDeque.pollLast();
        System.out.println(arrayDeque);
        System.out.println(arrayDeque.getFirst());
        System.out.println(arrayDeque.getLast());




    }
}
