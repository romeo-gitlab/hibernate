package collections;

import java.util.HashSet;
import java.util.Set;

public class HashSetEx1 {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("Zaur");
        set.add("Ivan");
        set.add("Izaura");
        set.add("Izaura");
        set.add(null);
        System.out.println(set.size());
        System.out.println(set.contains(null));
        set.remove(null);
        System.out.println(set.isEmpty());

        System.out.println(set);
        for (String s : set) {
            System.out.println(s);
        }

        HashSet<Integer> hashSet1 = new HashSet<>();
        hashSet1.add(5);
        hashSet1.add(3);
        hashSet1.add(8);
        hashSet1.add(0);
        System.out.println(hashSet1);

        HashSet<Integer> hashSet2 = new HashSet<>();
        hashSet2.add(7);
        hashSet2.add(3);
        hashSet2.add(8);
        hashSet2.add(10);
        System.out.println(hashSet2);


        HashSet<Integer> hashSet3 = new HashSet<>(hashSet1);
        hashSet3.addAll(hashSet2);
        System.out.println(hashSet3);
        HashSet<Integer> hashSet4 = new HashSet<>(hashSet2);
        hashSet4.retainAll(hashSet1);
        System.out.println(hashSet4);

        HashSet<Integer> hashSet5 = new HashSet<>(hashSet2);
        hashSet5.removeAll(hashSet1);
        System.out.println(hashSet5);




    }
}
