package collections;

import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetEx1 {
    public static void main(String[] args) {
        Set<Integer> set = new TreeSet<>();
        set.add(5);
        set.add(8);
        set.add(1);
        set.add(3);
        set.add(10);
        System.out.println(set);
        TreeSet<Student> treeSet = new TreeSet<>();
        Student student1 = new Student("Zaur", 3);
        Student student2 = new Student("Maria", 5);
        Student student3 = new Student("Sergey", 1);
        treeSet.add(student1);
        treeSet.add(student3);
        treeSet.add(student2);
        System.out.println(treeSet);
        System.out.println(treeSet.first());
        System.out.println(treeSet.last());
        Student student4 = new Student("Oleg", 4);
        Student student5 = new Student("Olly", 1);
        System.out.println(treeSet.headSet(student4));
        System.out.println(treeSet.tailSet(student4));
        System.out.println(treeSet.subSet(student5, student4));
        System.out.println(student3.equals(student5));
        System.out.println(student3.hashCode());
        System.out.println(student5.hashCode());

    }

    static class Student implements Comparable<Student> {
        String name;
        int course;

        public Student(String name, int course) {
            this.name = name;
            this.course = course;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Student student = (Student) o;
            return course == student.course; //сравнение имен убрали чтоб соблюдалось правило иквелс=тру, компэр=0
        }

        @Override
        public int hashCode() {
            return Objects.hash(course); //тоже убрали нэйм
        }

        @Override
        public int compareTo(Student other) {
            return this.course - other.course;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", course=" + course +
                    '}';
        }
    }
}

