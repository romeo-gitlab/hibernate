package collections;

import java.util.HashMap;
import java.util.Map;

public class HachMapEx1 {
    public static void main(String[] args) {
        Map<Integer, String> map1 = new HashMap<>();
        map1.put(1000, "Zaur");
        map1.put(1900, "Ivan");
        map1.put(2000, "Petr");
        map1.put(858, "Lisa");
        map1.put(1500, "Lisa");
        map1.putIfAbsent(1500, "Nick");
        map1.putIfAbsent(1510, "Petr");
        System.out.println(map1);
        System.out.println(map1.get(1000));
        map1.remove(1900);
        System.out.println(map1);
        System.out.println(map1.containsValue("Lisa"));
        System.out.println(map1.containsKey(null));
        System.out.println(map1.keySet());
        System.out.println(map1.values());


    }
}
