package Generic.igra;

public class test {
    public static void main(String[] args) {
        Scoolar scoolar1 = new Scoolar("Ivan", 13);
        Scoolar scoolar2 = new Scoolar("Petr", 15);
        Student student1 = new Student("Isaak", 20);
        Student student2 = new Student("Laura", 21);
        Employee employee1 = new Employee("Sergey", 33);
        Employee employee2 = new Employee("Irina", 43);

        Team<Scoolar> scoolarTeam = new Team<>("Dragons");
        scoolarTeam.addNewParticipant(scoolar1);
        scoolarTeam.addNewParticipant(scoolar2);
        //scoolarTeam.addNewParticipant(student1);

        Team<Student> studentTeam = new Team<>("Botanics");
        studentTeam.addNewParticipant(student1);
        studentTeam.addNewParticipant(student2);
        Team<Employee> employeeTeam = new Team<>("Workers");
        employeeTeam.addNewParticipant(employee1);
        employeeTeam.addNewParticipant(employee2);
//        Team<String> hitrojops = new Team<>("Hitrojops");
//        hitrojops.addNewParticipant("scoolar11");
        scoolarTeam.playWith(studentTeam);
        scoolarTeam.playWith(employeeTeam);
        studentTeam.playWith(employeeTeam);
    }
}
