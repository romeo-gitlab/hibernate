package Generic;

import java.util.ArrayList;
import java.util.List;

public class WildCard {
    public static void main(String[] args) {
        //List<?> list = new ArrayList<String>();
        List<Double> list1 = new ArrayList<>();
        list1.add(3.14);
        list1.add(3.19);
        list1.add(3.21);
        showList(list1);
        List<String> list2 = new ArrayList<>();
        list2.add("ok 3.14");
        list2.add("no");
        list2.add("yes");
        showList(list2);

        ArrayList<Double> arrayList = new ArrayList<>();
        arrayList.add(3.14);
        arrayList.add(3.19);
        arrayList.add(3.21);
        System.out.println(sum(arrayList));
    }
    static void showList(List<?> list) {
        System.out.println("Лист содержит элементы " + list);
    }

    public static double sum(List<? extends Number> al) {
        double sum = 0;
        for (Number n : al) {
            sum += n.doubleValue();
        }
        return sum;
    }
}

