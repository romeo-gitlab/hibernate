package enum_examples;

import java.util.Arrays;

public class Enum1 {
    public static void main(String[] args) {
        Today today = new Today(WeekDays.MONDAY);
        today.getInfo();
        WeekDays weekDays = WeekDays.FRIDAY;
        WeekDays weekDays2 = WeekDays.FRIDAY;
        System.out.println(weekDays==weekDays2);
        System.out.println(weekDays + ", it's " + weekDays.getMood());
        WeekDays [] array = WeekDays.values();
        System.out.println(Arrays.toString(array));
    }
}

enum WeekDays {
    MONDAY("bad"),
    TUESDAY("bad"),
    WEDNESDAY("so-so"),
    THURSDAY("so-so"),
    FRIDAY("good"),
    SATURDAY("great"),
    SUNDAY("good");

    private String mood;
    private WeekDays(String mood) {
        this.mood = mood;
    }

    public String getMood() {
        return mood;
    }
}

class Today {
    WeekDays weekDay;
    public Today (WeekDays weekDay) {
        this.weekDay = weekDay;
    }

    void getInfo() {
        switch (weekDay) {
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
            case FRIDAY:
                System.out.println("Go to work!");
                break;
            case SATURDAY:
            case SUNDAY:
                System.out.println("You can relax");
                break;
        }
        System.out.println("Mood in this day " + weekDay.getMood());
    }
}