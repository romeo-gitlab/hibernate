package anotation_examples;

import java.lang.annotation.*;

public class Test2 {
    public static void main(String[] args) throws ClassNotFoundException {
        Class xiomiClass = Class.forName("anotation_examples.Xiomi");
        Annotation annotation1 = xiomiClass.getAnnotation(Smartphone.class);
        Smartphone sm1 = (Smartphone) annotation1;
        System.out.println("Annotation info from Xiomi class " + sm1.OS() + ", " + sm1.yearOfCompanyCreation());

        Class iphoneClass = Class.forName("anotation_examples.Iphone");
        Annotation annotation2 = iphoneClass.getAnnotation(Smartphone.class);
        Smartphone sm2 = (Smartphone) annotation2;
        System.out.println("Annotation info from Iphone class " + sm2.OS() + ", " + sm2.yearOfCompanyCreation());
    }

}

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface Smartphone {
    String OS() default "Android";
    int yearOfCompanyCreation() default 2010;
}

@Smartphone
class Xiomi {
    String model;
    double price;
}
@Smartphone(OS = "IOS", yearOfCompanyCreation = 1976)
class Iphone {
    String model;
    double price;
}
